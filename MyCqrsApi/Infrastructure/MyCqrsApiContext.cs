﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using MyCqrsApi.Domain;

namespace MyCqrsApi.Infrastructure
{
    public class MyCqrsApiContext : DbContext
    {
        private readonly string _databaseName = Startup.DatabaseFile;

        public MyCqrsApiContext(DbContextOptions options)
            : base(options ?? throw new ArgumentNullException(nameof(options)))
        {
        }

        public DbSet<Student> Students { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder?.UseSqlite($"Filename={_databaseName}");
        }

    }
}
