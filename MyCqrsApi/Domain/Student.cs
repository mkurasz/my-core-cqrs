﻿using Newtonsoft.Json;

namespace MyCqrsApi.Domain
{
    public class Student
    {
        [JsonIgnore]
        public int StudentId { get; set; }

        public string FirstName { get; set; }     
        
        public string LastName { get; set; }

    }
}
