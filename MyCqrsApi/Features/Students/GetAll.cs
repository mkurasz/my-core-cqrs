﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MediatR;

namespace MyCqrsApi.Features.Students
{
    public class GetAll
    {
        public class Query : IRequest<IEnumerable<string>>
        {
        }

        public class QueryHandler : IRequestHandler<Query, IEnumerable<string>> {

            public async Task<IEnumerable<string>> Handle(Query request, CancellationToken cancellationToken)
            {
                var list = new List<string> {"123", "4545"};
                return list;
            }
        }
    }


}
